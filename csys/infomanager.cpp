/**
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include "infomanager.h"

#include "cpuinfo.h"
#include "diskinfo.h"
#include "memoryinfo.h"
#include "networkinfo.h"
#include "systeminfo.h"


InfoManager::InfoManager()
{
	ci = new CpuInfo();
	di = new DiskInfo();
	mi = new MemoryInfo();
	ni = new NetworkInfo();
	si = new SystemInfo();

	/** Need n-cpu lists */
	int nCPU = ci->getCpuCoreCount();
	for ( int i = 0; i < nCPU; i++ )
	{
		mCpuPercents << QList<double>();
		mCpuFrequencies << QList<double>();
	}

	/** Needs 3 lists */
	mCpuLoadAverages = { QList<double>(), QList<double>(), QList<double>() };

	/** Need 2 lists each */
	mNetworkUsage = { QList<double>(), QList<double>() };
	mNetworkSpeed = { QList<double>(), QList<double>() };
	mDiskUsage    = { QList<double>(), QList<double>() };
	mDiskSpeed    = { QList<double>(), QList<double>() };
}


InfoManager::~InfoManager()
{
	delete ci;
	delete di;
	delete mi;
	delete ni;
	delete si;
}


void InfoManager::setDataCount(int count)
{
	mDataPointsCount = count;
}


void InfoManager::setUpdateInterval(int msecs)
{
	mInterval = msecs;
}


void InfoManager::update()
{
	/** Update CPU Information */
	ci->updateInfo();

	QList<int>    xPercents     = ci->getCpuPercents();
	QList<double> xLoadAverages = ci->getLoadAvgs();
	QList<double> xFrequencies  = ci->getCPUFrequencies();

	int nCPU = ci->getCpuCoreCount();

	// If some more threads entererd into action
	// Assuming when cpu core can be started/stopped by tools like cpuFreq
	if (mCpuPercents.size() < nCPU)
	{
		int count = nCPU - mCpuPercents.size();
		for ( int i = 0; i < count; i++ )
		{
			mCpuPercents << QList<double>();
			mCpuFrequencies << QList<double>();
		}
	}

	for ( int i = 0; i < nCPU; i++ )
	{
		mCpuPercents[i] << xPercents[i];
		mCpuFrequencies[i] << xFrequencies[i];

		while (mCpuPercents[i].count() > mDataPointsCount)
		{
			mCpuPercents[i].removeFirst();
		}
		while (mCpuFrequencies[i].count() > mDataPointsCount)
		{
			mCpuFrequencies[i].removeFirst();
		}
	}

	// When CPU core are not active, insert zero
	for (int i = nCPU; i < mCpuPercents.count(); i++)
	{
		mCpuPercents[i] << 0;
		mCpuFrequencies[i] << 0;

		while (mCpuPercents[i].count() > mDataPointsCount)
		{
			mCpuPercents[i].removeFirst();
		}
		while (mCpuFrequencies[i].count() > mDataPointsCount)
		{
			mCpuFrequencies[i].removeFirst();
		}
	}

	for ( int i = 0; i < xLoadAverages.count(); i++ )
	{
		mCpuLoadAverages[i] << xLoadAverages[i];
		while (mCpuLoadAverages[i].count() > mDataPointsCount)
		{
			mCpuLoadAverages[i].removeFirst();
		}
	}

	/** Update Memory Information */
	mi->updateMemoryInfo();

	mRamUsage << mi->getMemUsed();
	mSwapUsage << mi->getSwapUsed();

	while (mRamUsage.count() > mDataPointsCount)
	{
		mRamUsage.removeFirst();
	}

	while (mSwapUsage.count() > mDataPointsCount)
	{
		mSwapUsage.removeFirst();
	}

	/** Network Usage */
	QList<quint64> iobytes = ni->getIOBytes();
	double         dlSpeed = (iobytes[0] - (mNetworkUsage[0].count() ? mNetworkUsage[0].last() : iobytes[0])) / (mInterval / 1000.0);
	double         ulSpeed = (iobytes[1] - (mNetworkUsage[1].count() ? mNetworkUsage[1].last() : iobytes[1])) / (mInterval / 1000.0);

	mNetworkUsage[0] << iobytes[0];
	mNetworkUsage[1] << iobytes[1];

	mNetworkSpeed[0] << dlSpeed;
	mNetworkSpeed[1] << ulSpeed;

	for ( int i = 0; i < 2; i++ )
	{
		while (mNetworkUsage[i].count() > mDataPointsCount)
		{
			mNetworkUsage[i].removeFirst();
		}
		while (mNetworkSpeed[i].count() > mDataPointsCount)
		{
			mNetworkSpeed[i].removeFirst();
		}
	}

	/** Disks Usage */
	QList<quint64> disksIO = di->getDiskIO();
	double         rSpeed  = (disksIO[0] - (mDiskUsage[0].count() ? mDiskUsage[0].last() : disksIO[0])) / (mInterval / 1000.0);
	double         wSpeed  = (disksIO[1] - (mDiskUsage[1].count() ? mDiskUsage[1].last() : disksIO[1])) / (mInterval / 1000.0);

	mDiskUsage[0] << disksIO[0];
	mDiskUsage[1] << disksIO[1];

	mDiskSpeed[0] << rSpeed;
	mDiskSpeed[1] << wSpeed;

	for ( int i = 0; i < 2; i++ )
	{
		while (mDiskUsage[i].count() > mDataPointsCount)
		{
			mDiskUsage[i].removeFirst();
		}
		while (mDiskSpeed[i].count() > mDataPointsCount)
		{
			mDiskSpeed[i].removeFirst();
		}
	}
}


/*
  * CPU Provider
  */
quint8 InfoManager::getCpuCoreCount() const
{
	return ci->getCpuCoreCount();
}


QList<QList<double> > InfoManager::getCpuPercents(int count) const
{
	if (count == 0)
	{
		return mCpuPercents;
	}

	int mid = mCpuPercents[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mCpuPercents.count(); i++ )
	{
		retVal << mCpuPercents[i].mid(mid);
	}

	return retVal;
}


QList<QList<double> > InfoManager::getCpuLoadAvgs(int count) const
{
	if (count == 0)
	{
		return mCpuLoadAverages;
	}

	int mid = mCpuLoadAverages[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mCpuLoadAverages.count(); i++ )
	{
		retVal << mCpuLoadAverages[i].mid(mid);
	}

	return retVal;
}


QList<QList<double> > InfoManager::getCpuFrequencies(int count) const
{
	if (count == 0)
	{
		return mCpuFrequencies;
	}

	int mid = mCpuFrequencies[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mCpuFrequencies.count(); i++ )
	{
		retVal << mCpuFrequencies[i].mid(mid);
	}

	return retVal;
}


QStringList InfoManager::getCpuFrequenciesStr() const
{
	return ci->getCPUFrequenciesStr();
}


/*
  * Memory Provider
  */
QList<double> InfoManager::getMemUsed(int count) const
{
	if (count == 0)
	{
		return mRamUsage;
	}

	int mid = mRamUsage.count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	return mRamUsage.mid(mid);
}


quint64 InfoManager::getMemTotal() const
{
	return mi->getMemTotal();
}


QList<double> InfoManager::getSwapUsed(int count) const
{
	if (count == 0)
	{
		return mSwapUsage;
	}

	int mid = mSwapUsage.count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	return mSwapUsage.mid(mid);
}


quint64 InfoManager::getSwapTotal() const
{
	return mi->getSwapTotal();
}


/*
  * Network Provider
  */
QList<QList<double> > InfoManager::getNetworkUsage(int count) const
{
	if (count == 0)
	{
		return mNetworkUsage;
	}

	int mid = mNetworkUsage[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mNetworkUsage.count(); i++ )
	{
		retVal << mNetworkUsage[i].mid(mid);
	}

	return retVal;
}


QList<QList<double> > InfoManager::getNetworkSpeed(int count) const
{
	if (count == 0)
	{
		return mNetworkSpeed;
	}

	int mid = mNetworkSpeed[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mNetworkSpeed.count(); i++ )
	{
		retVal << mNetworkSpeed[i].mid(mid);
	}

	return retVal;
}


/*
  * Disk Provider
  */
QList<Disk> InfoManager::getDisks() const
{
	return di->getDisks();
}


QList<QList<double> > InfoManager::getDiskUsage(int count) const
{
	if (count == 0)
	{
		return mDiskUsage;
	}

	int mid = mDiskUsage[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mDiskUsage.count(); i++ )
	{
		retVal << mDiskUsage[i].mid(mid);
	}

	return retVal;
}


QList<QList<double> > InfoManager::getDiskSpeed(int count) const
{
	if (count == 0)
	{
		return mDiskSpeed;
	}

	int mid = mDiskSpeed[0].count() - count;

	if (mid < 0)
	{
		mid = 0;
	}

	QList<QList<double> > retVal;

	for ( int i = 0; i < mDiskSpeed.count(); i++ )
	{
		retVal << mDiskSpeed[i].mid(mid);
	}

	return retVal;
}


/*
  * System Provider
  */
QFileInfoList InfoManager::getCrashReports() const
{
	return si->getCrashReports();
}


QFileInfoList InfoManager::getAppLogs() const
{
	return si->getAppLogs();
}


QFileInfoList InfoManager::getAppCaches() const
{
	return si->getAppCaches();
}


QString InfoManager::getUserName() const
{
	return si->getUsername();
}


void InfoManager::resetCPUUsages()
{
	for (int i = 0; i < mCpuPercents.size(); i++)
	{
		mCpuPercents[i].clear();
	}

	for (int i = 0; i < mCpuLoadAverages.size(); i++)
	{
		mCpuLoadAverages[i].clear();
	}

	for (int i = 0; i < mCpuFrequencies.size(); i++)
	{
		mCpuFrequencies[i].clear();
	}
}


void InfoManager::resetMemoryUsages()
{
	mRamUsage.clear();
	mSwapUsage.clear();
}


void InfoManager::resetNetworkUsages()
{
	for (int i = 0; i < mNetworkUsage.size(); i++)
	{
		mNetworkUsage[i].clear();
	}

	for (int i = 0; i < mNetworkSpeed.size(); i++)
	{
		mNetworkSpeed[i].clear();
	}
}


void InfoManager::resetDiskUsages()
{
	for (int i = 0; i < mDiskSpeed.size(); i++)
	{
		mDiskSpeed[i].clear();
	}

	for (int i = 0; i < mDiskUsage.size(); i++)
	{
		mDiskUsage[i].clear();
	}
}


void InfoManager::resetAllUsages()
{
	resetCPUUsages();
	resetMemoryUsages();
	resetNetworkUsages();
	resetDiskUsages();
}
