/**
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019-2023 CuboCore Group
  *
  * NOTE: Several classes and functions have been used here, which are from LXQt.
  * Below is their copyright header. LXQt functions (modified suitably) have a
  * comment above them.
  * Any bugs in this entire program are to be reported to us, and not to LXQt.
  *
  * BEGIN_COMMON_COPYRIGHT_HEADER
  * (c)LGPL2+
  *
  * LXQt - a lightweight, Qt based, desktop toolset
  * https://lxqt.org
  *
  * Copyright: 2010-2011 Razor team
  * Authors:
  *   Alexander Sokoloff <sokoloff.a@gmail.com>
  *   Petr Vanek <petr@scribus.info>
  *
  * This program or library is free software; you can redistribute it
  * and/or modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2.1 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General
  * Public License along with this library; if not, write to the
  * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  * Boston, MA 02110-1301 USA
  *
  * END_COMMON_COPYRIGHT_HEADER
  *
  * Other parts of the program are developed independent of LXQt and are
  * released under GLP v3+
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include "power.h"

#define CONSOLEKIT_SERVICE      "org.freedesktop.ConsoleKit"
#define CONSOLEKIT_PATH         "/org/freedesktop/ConsoleKit/Manager"
#define CONSOLEKIT_INTERFACE    "org.freedesktop.ConsoleKit.Manager"

#define SYSTEMD_SERVICE         "org.freedesktop.login1"
#define SYSTEMD_PATH            "/org/freedesktop/login1"
#define SYSTEMD_INTERFACE       "org.freedesktop.login1.Manager"

/** LXQt fucntion: this function was taken from LXQt sources, and suitably modified */
static bool dbusCallSystemd(QDBusInterface *dbus, const QString& method, bool needBoolArg)
{
	if (not dbus->isValid())
	{
		qWarning() << "dbusCall: QDBusInterface is invalid" << dbus->service() << dbus->path() << dbus->interface() << method;
		return false;
	}

	QDBusMessage msg;
	if (needBoolArg)
	{
		msg = dbus->call(method, QVariant(true));
	}
	else
	{
		msg = dbus->call(method);
	}

	if (not msg.errorName().isEmpty())
	{
		qWarning() << "Error name " << msg.errorName();
		qWarning() << "Error msg  " << msg.errorMessage();
	}

	QList<QVariant> args = msg.arguments();

	// If the method no returns value, we believe that it was successful.
	if (args.isEmpty() or args.first().isNull())
	{
		return true;
	}

	QString response = args.first().toString();

	qDebug() << "systemd:" << method << "=" << response;

	return response == QLatin1String("yes") || response == QLatin1String("challenge");
}


Power::Power(QObject *parent) : QObject(parent)
{
	createPowerProviders();
}


Power::~Power()
{
	while (providers.empty())
	{
		delete providers.takeFirst();
	}
}


void Power::createPowerProviders()
{
	// 1: SystemD

	auto iface = new QDBusInterface(SYSTEMD_SERVICE, SYSTEMD_PATH, SYSTEMD_INTERFACE, QDBusConnection::systemBus(), this);

	if (iface->isValid())
	{
		providers << iface;
	}

	// 2: ConsoleKit

	iface = new QDBusInterface(CONSOLEKIT_SERVICE, CONSOLEKIT_PATH, CONSOLEKIT_INTERFACE, QDBusConnection::systemBus(), this);

	if (iface->isValid())
	{
		providers << iface;
	}
}


bool Power::systemCanSuspend()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		return dbusCallSystemd(iface, "CanSuspend", false);
	});
}


bool Power::systemCanHibernate()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		return dbusCallSystemd(iface, "CanHibernate", false);
	});
}


bool Power::systemCanReboot()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		return dbusCallSystemd(iface, "CanReboot", false);
	});
}


bool Power::systemCanHalt()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		return dbusCallSystemd(iface, "CanPowerOff", false);
	});
}


bool Power::systemSuspend()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		if (dbusCallSystemd(iface, "CanSuspend", false))
		{
			return dbusCallSystemd(iface, "Suspend", true);
		}
		return false;
	});
}


bool Power::systemHibernate()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		if (dbusCallSystemd(iface, "CanHibernate", false))
		{
			return dbusCallSystemd(iface, "Hibernate", true);
		}
		return false;
	});
}


bool Power::systemReboot()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		if (dbusCallSystemd(iface, "CanReboot", false))
		{
			return dbusCallSystemd(iface, "Reboot", true);
		}
		return false;
	});
}


bool Power::systemHalt()
{
	return std::any_of(providers.begin(), providers.end(), [](QDBusInterface *iface) {
		if (dbusCallSystemd(iface, "CanPowerOff", false))
		{
			return dbusCallSystemd(iface, "PowerOff", true);
		}
		return false;
	});
}
