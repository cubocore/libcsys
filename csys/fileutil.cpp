/**
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  **/


#include <QFile>
#include <QTextStream>

#include "fileutil.h"


QString FileUtil::readStringFromFile(const QString& path, const QIODevice::OpenMode& mode)
{
	QFile   file(path);
	QString data = "";

	if (file.open(mode))
	{
		QTextStream out(&file);
		data = out.readAll();
		file.close();
	}

	return data;
}


QStringList FileUtil::readListFromFile(const QString& path, const QIODevice::OpenMode& mode)
{
	QString fileStr = FileUtil::readStringFromFile(path, mode).trimmed();

	if (fileStr.length())
	{
		return fileStr.split("\n");
	}

	return QStringList();
}


bool FileUtil::writeFile(const QString& path, const QString& content, const QIODevice::OpenMode& mode)
{
	QFile file(path);

	if (file.open(mode))
	{
		QTextStream in(&file);
		in << content;
		file.close();
		return true;
	}

	return false;
}
